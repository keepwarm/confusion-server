const express = require('express');
const bodyParser = require('body-parser');

const promoRouter = express.Router();
promoRouter.use(bodyParser.json());

promoRouter.route('/')
.all((req,res,next) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req,res,next) => {
    res.end('Will send all the promotions to you!');
})
.post((req,res,next) => {
    res.end('Will add the promotion: ' + req.body.name + 'with promotions: ' + req.body.description);
})
.put((req,res,next) => {
    res.statusCode = 403; 
    res.end('PUT operation not supported on /promo');
})
.delete((req,res,next) => {
    res.end('Deleting all the dishes!');
});

promoRouter.route('/:promoId')
.all((req,res,next) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req,res,next) => {
    res.end('Will send promotion with Id: ' + req.params.promoId + ' to you!');
})
.post((req,res,next) => {
    res.end('POST operation not supported on /: id' + req.params.promoId);
})
.put((req,res,next) => {
    res.statusCode = 403;
    res.end('Updating promotion with id: ' + req.params.promoId + ' with details: ' + JSON.stringify(req.body));
})
.delete((req,res,next) => {
    res.end('Deleting promotion with id: ' + req.params.promoId);
});


module.exports = promoRouter;