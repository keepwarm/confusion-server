const express = require('express');
const bodyParser = require('body-parser');

const leaderRouter = express.Router();
leaderRouter.use(bodyParser.json());

leaderRouter.route('/')
.all((req,res,next) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req,res,next) => {
    res.end('Will send all the leaders to you!');
})
.post((req,res,next) => {
    res.end('Will add the leader: ' + req.body.name + 'with leader: ' + req.body.description);
})
.put((req,res,next) => {
    res.statusCode = 403; 
    res.end('PUT operation not supported on /leader');
})
.delete((req,res,next) => {
    res.end('Deleting all the leaders!');
});

leaderRouter.route('/:leaderId')
.all((req,res,next) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req,res,next) => {
    res.end('Will send leader with Id: ' + req.params.leaderId + ' to you!');
})
.post((req,res,next) => {
    res.end('POST operation not supported on /: id' + req.params.leaderId);
})
.put((req,res,next) => {
    res.statusCode = 403;
    res.end('Updating leader with id: ' + req.params.leaderId + ' with details: ' + JSON.stringify(req.body));
})
.delete((req,res,next) => {
    res.end('Deleting leader with id: ' + req.params.leaderId);
});


module.exports = leaderRouter;